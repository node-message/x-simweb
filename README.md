<h1>x-simweb</h1>

> 为开发者提供了一个简单的Http服务器，可将本地静态文件进行挂载，还支持服务的proxy反向代理。

## 简介

```shell
  ____ ___ __  ____        _______ ____
 / ___|_ _|  \/  \ \      / / ____| __ )
 \___ \| || |\/| |\ \ /\ / /|  _| |  _ \
  ___) | || |  | | \ V  V / | |___| |_) |
 |____/___|_|  |_|  \_/\_/  |_____|____/ v1.0.0.alpha

[ simweb ]2024/06/13 11:47:28 日志文件路径: D:\pro\simweb\bin\simweb-win\simweb-2024-06-13.log
Usage:
  simweb.exe [OPTIONS] <natclient | natserver | web>

Help Options:
  /?          Show this help message
  /h, /help   Show this help message

Available commands:
  natclient  内网穿透客户端
  natserver  内网穿透服务端
  web        web服务器
```

## 开始使用

### web

```shell
Usage:
  simweb.exe [OPTIONS] web [web-OPTIONS]

Help Options:
  /?                      Show this help message
  /h, /help               Show this help message

[web command options]
      /p, /port:          启动的端口 (default: 80)
      /d, /direct:        映射的文件目录 (default: D:\pro\simweb\bin\simweb-win)
      /t, /target:        proxy的服务地址
      /r, /request-path:  服务前缀 (default: /)
      /c, /config:        配置文件参数详见:https://gitee.com/node-message/x-simweb/blob/master/doc/config.md
```

1. 下面将使用一个简单的示例，将本地目录**E:\system**挂载到8810的"/test"路径下。

```shell
simweb web -p 8810 -d E:\system -r=/test/

[ simweb ]2024/06/05 22:42:16 服务端口:  8810
[ simweb ]2024/06/05 22:42:16 服务前缀: /test/
[ simweb ]2024/06/05 22:42:16 服务目录:  E:\system
```

2. 接下来使用8811端口，将"/bd/"路径,挂载到百度的官网,https://www.baidu.com/。

```shell
simweb web -p 8811 -r=/bd/ -t https://www.baidu.com/

[ simweb ]2024/06/05 22:46:48 服务端口:  8811
[ simweb ]2024/06/05 22:46:48 服务前缀: /bd/
[ simweb ]2024/06/05 22:46:48 proxy地址:  https://www.baidu.com/
```

3. 然后我们使用配置文件进行一次服务的映射，将上面两个内容通过配置文件进行统一配置，并在8812端口展示。配置文件**config.yml
   **:

```yml
port: 8812
files:
  - target: E:\system
    contextPath: /test/
servers:
  - target:
      - https://www.baidu.com/
    contextPath: /bd/
```

    将配置文件内容填写，进行启动即可。

```shell
simweb web -c E:\system\config.yml

[ simweb ]2024/06/05 22:57:35 服务端口:  8812
[ simweb ]2024/06/05 22:57:35 服务目录:  E:\system .服务前缀:  /test/
[ simweb ]2024/06/05 22:57:35 proxy地址:  https://www.baidu.com/ .服务前缀:  /bd/
```

### nat

![nat.png](doc/img/nat.png)