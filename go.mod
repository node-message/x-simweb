module nodemessage.com/x-simweb

go 1.20

require (
	github.com/jessevdk/go-flags v1.5.0
	golang.org/x/text v0.16.0
	gopkg.in/yaml.v2 v2.4.0
)

require golang.org/x/sys v0.20.0 // indirect
