//go:build linux

package logs

import (
	"fmt"
	"io"
	"log"
	"nodemessage.com/x-simweb/utils"
	"os"
	"path/filepath"
)

var Path = ""

func Logs(suffix string) *log.Logger {
	Path = filepath.Join(utils.BinAbsPath(), fmt.Sprintf("simweb-%s.log", suffix))
	logger := log.Default()
	logger.SetPrefix("[ simweb ]")
	file, err := os.OpenFile(Path, os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		panic(fmt.Errorf("日志文件创建失败:%v", err))
	}
	logger.SetOutput(io.MultiWriter(os.Stdout, file))
	return logger
}
