//go:build windows

package logs

import (
	"fmt"
	"golang.org/x/text/encoding/simplifiedchinese"
	"golang.org/x/text/transform"
	"io"
	"log"
	"nodemessage.com/x-simweb/utils"
	"os"
	"path/filepath"
)

var Path = ""

func Logs(suffix string) *log.Logger {
	Path = filepath.Join(utils.BinAbsPath(), fmt.Sprintf("simweb-%s.log", suffix))
	logger := log.Default()
	logger.SetPrefix("[ simweb ]")
	file, err := os.OpenFile(Path, os.O_CREATE|os.O_APPEND, os.ModePerm)
	if err != nil {
		panic(fmt.Errorf("日志文件创建失败:%v", err))
	}
	fileWriter := transform.NewWriter(file, simplifiedchinese.GBK.NewEncoder())
	logger.SetOutput(io.MultiWriter(os.Stdout, fileWriter))
	return logger
}
