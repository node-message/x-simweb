package web

type Config struct {
	/* 端口 */
	Port int `yaml:"port"`
	/* 域名 */
	Domain string `yaml:"domain"`
	/* proxy服务 */
	Servers []Server `yaml:"servers"`
	/* 静态文件路径 */
	Files []File `yaml:"files"`
}

type Server struct {
	/* 服务地址 */
	Target []string `yaml:"target"`
	/* 服务前缀 */
	ContextPath string `yaml:"contextPath"`
}

type File struct {
	/* 目录地址 */
	Target string `yaml:"target"`
	/* 服务前缀 */
	ContextPath string `yaml:"contextPath"`
}
