package web

import (
	"fmt"
	"log"
	"net"
	"net/http"
	"nodemessage.com/x-simweb/build"
	"nodemessage.com/x-simweb/logic"
	"nodemessage.com/x-simweb/utils"
	"strings"
	"time"
)

type Opt struct {
	Port        int      `short:"p" long:"port" description:"启动的端口"`
	Home        string   `short:"d" long:"direct" description:"映射的文件目录"`
	Target      []string `short:"t" long:"target" description:"proxy的服务地址"`
	RequestPath string   `short:"r" long:"request-path" description:"服务前缀"`
	Config      string   `short:"c" long:"config" description:"配置文件参数详见:https://gitee.com/node-message/x-simweb/blob/master/doc/config.md"`
}

func (receiver Opt) Execute(arg []string) error {
	start(receiver, build.Log)
	return nil
}

func start(opts Opt, simLog *log.Logger) {
	(&opts).Port = randomPort(simLog, opts.Port)
	var wait chan int
	if opts.Config != "" {
		yml := &Config{}
		err := utils.ReadYml(opts.Config, yml)
		if err != nil {
			simLog.Println("配置文件读取异常:", err)
			return
		}
		go startWebByConfig(simLog, yml, wait)
		simLog.Println("服务端口: ", yml.Port)
	} else {
		go startWeb(simLog, opts, wait)
		simLog.Println("服务端口: ", opts.Port)
	}
	select {
	case <-wait:
		simLog.Println("停止服务.")
	}
}

type ResponseWriter struct {
	http.ResponseWriter
	statusCode int
}

func (rw *ResponseWriter) WriteHeader(code int) {
	rw.statusCode = code
	rw.ResponseWriter.WriteHeader(code)
}

func startWebByConfig(simLog *log.Logger, config *Config, wait chan int) {
	for _, f := range config.Files {
		if f.ContextPath == "" {
			f.ContextPath = "/"
		}
		simLog.Println("服务目录: ", f.Target, ".服务前缀: ", f.ContextPath)
		http.Handle(f.ContextPath, http.StripPrefix(f.ContextPath,
			loggingMiddleware(simLog, http.FileServer(http.Dir(f.Target)))))
	}
	for _, s := range config.Servers {
		if s.ContextPath == "" {
			s.ContextPath = "/"
		}
		simLog.Println("proxy地址: ", strings.Join(s.Target, ","), ".服务前缀: ", s.ContextPath)
		proxy := logic.NewReverseProxy(s.Target, s.ContextPath)
		http.HandleFunc(s.ContextPath, loggingFuncMiddleware(simLog, proxy.ServeHTTP))
	}
	err := http.ListenAndServe(fmt.Sprintf(":%d", config.Port), nil)
	if err != nil {
		simLog.Println("服务启动失败:", err)
		return
	}
	wait <- 0
}

func startWeb(simLog *log.Logger, opts Opt, wait chan int) {
	simLog.Println("服务前缀:", opts.RequestPath)
	if len(opts.Target) > 0 {
		simLog.Println("proxy地址: ", strings.Join(opts.Target, ","))
		proxy := logic.NewReverseProxy(opts.Target, opts.RequestPath)
		http.HandleFunc(opts.RequestPath, loggingFuncMiddleware(simLog, proxy.ServeHTTP))
	} else {
		simLog.Println("服务目录: ", opts.Home)
		http.Handle(opts.RequestPath, http.StripPrefix(opts.RequestPath,
			loggingMiddleware(simLog, http.FileServer(http.Dir(opts.Home)))))
	}
	err := http.ListenAndServe(fmt.Sprintf(":%d", opts.Port), nil)
	if err != nil {
		simLog.Println("服务启动失败:", err)
		return
	}
	wait <- 0
}

func loggingFuncMiddleware(simLog *log.Logger, next http.HandlerFunc) http.HandlerFunc {
	return loggingMiddleware(simLog, next)
}

func loggingMiddleware(simLog *log.Logger, next http.Handler) http.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		now := time.Now()
		writer := ResponseWriter{ResponseWriter: w, statusCode: -1}
		next.ServeHTTP(&writer, r)
		simLog.Printf("[%s - %s] status:%d | durationTime:%dms | remoteAddr:%s | userAgent:%s", r.Method, r.URL.Path, writer.statusCode, time.Since(now).Milliseconds(), r.RemoteAddr, r.UserAgent())
	}
}

func randomPort(simLog *log.Logger, port int) int {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", port))
	if err != nil {
		simLog.Println("默认端口80被占用,可使用'-p xx'指定端口")
		// 如果端口 81 被占用，则使用随机端口
		listener, err = net.Listen("tcp", ":0")
		if err != nil {
			panic(err)
		}
	}
	defer listener.Close()
	return listener.Addr().(*net.TCPAddr).Port
}
