package logic

import (
	"log"
	"net"
	"net/http"
	"net/http/httputil"
	"net/url"
	"strings"
	"time"
)

type ReverseProxy struct {
	targets     []*url.URL
	counter     uint32
	contextPath string
}

func NewReverseProxy(targets []string, contextPath string) *ReverseProxy {
	urls := make([]*url.URL, len(targets))
	for i, target := range targets {
		url, err := url.Parse(target)
		if err != nil {
			log.Fatalf("Failed to parse target URL %s: %v", target, err)
		}
		urls[i] = url
	}
	return &ReverseProxy{targets: urls, contextPath: contextPath}
}

func (p *ReverseProxy) getNextTarget() *url.URL {
	return p.targets[time.Now().Second()%len(p.targets)]
}

func (p *ReverseProxy) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	target := p.getNextTarget()
	proxy := httputil.NewSingleHostReverseProxy(target)
	proxy.Transport = &http.Transport{
		Proxy: http.ProxyFromEnvironment,
		DialContext: (&net.Dialer{
			Timeout:   30 * time.Second,
			KeepAlive: 30 * time.Second,
		}).DialContext,
		MaxIdleConns:          100,
		IdleConnTimeout:       90 * time.Second,
		TLSHandshakeTimeout:   10 * time.Second,
		ExpectContinueTimeout: 1 * time.Second,
	}
	r.Host = target.Host
	if p.contextPath != "" && p.contextPath != "/" {
		r.URL.Path = strings.Replace(r.URL.Path, p.contextPath, "", 1)
	}
	proxy.ServeHTTP(w, r)
}
