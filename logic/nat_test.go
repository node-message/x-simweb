package logic

import (
	"bufio"
	"fmt"
	"io"
	"net"
	"nodemessage.com/x-simweb/logic/nat"
	"nodemessage.com/x-simweb/logic/nat/clietn"
	"nodemessage.com/x-simweb/logic/nat/server"
	"nodemessage.com/x-simweb/logs"
	"path/filepath"
	"testing"
	"time"
)

var logger = logs.Logs(time.Now().Format(time.DateOnly))

func TestA(t *testing.T) {
	s := server.NewNatServer(logger).SetRsaKeyPath(filepath.Dir("./"))
	err := s.GenRsaKeys(true)
	if err != nil {
		logger.Println(err)
		return
	}
}

func TestKey(t *testing.T) {
	connect := nat.NewConnect()
	connect.Uuid = "123"
	json, _ := nat.EncodeDataJson(logger, connect, filepath.Dir("./"))
	println("数据:", json)
	m := nat.NewConnect()
	nat.DecodeDataJson(logger, []byte(json), m, filepath.Dir("./"))
	fmt.Printf("结果%v", m)
}

func TestNatServer(t *testing.T) {
	server.NewNatServer(logger).
		SetRsaKeyPath(filepath.Dir("./")).
		SetConnectPort(8001).
		SetVirtualPort(8080).
		Run()
}

func TestNatClient(t *testing.T) {
	client := clietn.NewNatClient(logger, "127.0.0.1", 8001)
	for i := 0; i < 10; i++ {
		i := i
		go func() {
			client.
				SetRsaKeyPath(filepath.Dir("./")).
				RunWork(nat.TypeDomain, fmt.Sprintf("127.0.0.%d:8080", i), "127.0.0.1:8081")
		}()
	}
	select {}
}

func TestProxy(t *testing.T) {
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", 8080))
	if err != nil {
		logger.Printf("启动虚拟端口监听异常:%v\n", err)
		return
	}
	defer listener.Close()
	logger.Println("nat virtualServer启动成功:", 8080)
	for {
		clientTcp, err := listener.Accept()
		if err != nil {
			logger.Printf("nat virtualServer,监听到客户端,处理异常,%v\n", err)
			continue
		}
		go func() {
			defer clientTcp.Close()
			reader := bufio.NewReader(clientTcp)
			writer := bufio.NewWriter(clientTcp)
			proxy, err := net.Dial("tcp", ":8081")
			if err != nil {
				logger.Printf("连接proxy:%s失败,%v\n", "8081", err)
				return
			}
			defer proxy.Close()
			go func() {
				io.Copy(proxy, reader)
			}()
			io.Copy(writer, proxy)
		}()
	}
}
