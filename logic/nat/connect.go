package nat

import (
	"bufio"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"log"
	"net"
	"nodemessage.com/x-simweb/utils"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

const (
	KeysRelativeDir = "rsa"
)

type ClientType int

const (
	TypePort   ClientType = iota
	TypeDomain            = iota
)

type AuthPack struct {
	Uuid              string
	HeartbeatPort     int
	HeartbeatInterval time.Duration
	Key               string
}

type Connect struct {
	Uuid string

	Slat         string
	Ins          net.Conn
	ClientType   ClientType
	Val          any
	ListenerPort int
	heartbeat    int32
}

func NewConnect() *Connect {
	return &Connect{}
}

func NewServiceConnect(slat string, clientType ClientType, val any, listenerPort int) *Connect {
	return &Connect{Slat: slat, ClientType: clientType, Val: val, ListenerPort: listenerPort}
}

func (c *Connect) isSlat(slat string) bool {
	return c.Slat != "" && slat == c.Slat
}

func (c *Connect) HeartbeatProcess() {
	if c.heartbeat < 0 {
		c.heartbeat = 0
	} else {
		atomic.AddInt32(&c.heartbeat, 1)
	}
}

func (c *Connect) HeartbeatUnProcess(limit int32) bool {
	return atomic.AddInt32(&c.heartbeat, -1)+limit >= 0
}

type Heartbeat struct {
	addr     string
	logger   *log.Logger
	jumpLock sync.Mutex
}

func NewServiceHeartbeat(logger *log.Logger) *Heartbeat {
	return &Heartbeat{addr: fmt.Sprintf(":0"), logger: logger}
}

func NewClientHeartbeat(host string, port int, logger *log.Logger) *Heartbeat {
	return &Heartbeat{addr: fmt.Sprintf("%s:%d", host, port), logger: logger}
}

func (h *Heartbeat) Listener(callback func(data string), portChan chan int) error {
	addr, err := net.ResolveUDPAddr("udp", h.addr)
	if err != nil {
		return err
	}
	udp, err := net.ListenUDP("udp", addr)
	if err != nil {
		return err
	}
	defer udp.Close()
	portChan <- udp.LocalAddr().(*net.UDPAddr).Port
	reader := bufio.NewReader(udp)
	for {
		line, _, err := reader.ReadLine()
		if err != nil {
			h.logger.Printf("%s:心跳包接收消息异常.%v\n", h.addr, err)
			continue
		}
		go callback(strings.TrimRight(string(line), "\n"))
	}

}

func (h *Heartbeat) StartJump(interval time.Duration, msg string) error {
	if !h.jumpLock.TryLock() {
		return fmt.Errorf("已存在心跳")
	}
	defer h.jumpLock.Unlock()
	go h.jump(interval, msg)
	return nil
}

func (h *Heartbeat) jump(interval time.Duration, msg string) {
	time.AfterFunc(interval, func() {
		h.Ping(msg)
		go h.jump(interval, msg)
	})
}

func (h *Heartbeat) Ping(msg string) {
	if !strings.HasSuffix(msg, "\n") {
		msg = msg + "\n"
	}
	addr, err := net.ResolveUDPAddr("udp", h.addr)
	if err != nil {
		h.logger.Printf("%s地址解析失败.%v\n", h.addr, err)
		return
	}
	udp, err := net.DialUDP("udp", nil, addr)
	if err != nil {
		h.logger.Printf("%s地址连接失败.%v\n", h.addr, err)
		return
	}
	defer udp.Close()
	writer := bufio.NewWriter(udp)
	_, err = writer.WriteString(msg)
	writer.Flush()
	if err != nil {
		h.logger.Printf("%s消息写入失败.%v\n", h.addr, err)
		return
	}
}

func EncodeDataJson(logger *log.Logger, data interface{}, path string) (string, error) {
	var marshal []byte
	if dataStr, ok := data.(string); ok {
		_, err := json.Marshal(dataStr)
		if err != nil {
			logger.Printf("数据序列化Json异常:%v\n", data)
			return "", err
		}
		marshal = []byte(dataStr)
	} else {
		marshalLocal, err := json.Marshal(data)
		if err != nil {
			logger.Printf("数据序列化Json异常:%v\n", data)
			return "", err
		}
		marshal = marshalLocal
	}
	// todo 缓存 读取一次即可
	publicKeyDir := filepath.Join(path, KeysRelativeDir, "publicKey")
	if !utils.FileIsExist(publicKeyDir) {
		logger.Println("密钥文件不存在:", publicKeyDir)
		return "", ErrorRsaKeysNotfound()
	}
	key, err := utils.LoadPublicKey(publicKeyDir)
	if err != nil {
		logger.Println("密钥文件读取异常:", publicKeyDir)
		return "", err
	}
	encrypt, err := utils.Encrypt(key, marshal)
	if err != nil {
		logger.Println("数据加密异常:", string(marshal))
		return "", err
	}
	return hex.EncodeToString(encrypt), nil
}

func DecodeDataJson(logger *log.Logger, jsonData []byte, outputBean interface{}, path string) error {
	decodeHexString, err := hex.DecodeString(string(jsonData))
	if err != nil {
		logger.Println("ras数据转化异常:", string(jsonData))
		return err
	}
	keysDir := filepath.Join(path, KeysRelativeDir)
	privateKeyPath := filepath.Join(keysDir, "privateKey")
	key, err := utils.LoadPrivateKey(privateKeyPath)
	if err != nil {
		logger.Println("ras私钥读取异常", err)
		return err
	}
	decrypt, err := utils.Decrypt(key, decodeHexString)
	if err != nil {
		logger.Println("ras数据解密异常", err)
		return err
	}
	err = json.Unmarshal(decrypt, outputBean)
	if err != nil {
		logger.Println("ras数据反序列化Json异常:", string(decrypt))
		return err
	}
	return nil
}
