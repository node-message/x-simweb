package server

import (
	"bufio"
	"context"
	"fmt"
	"io"
	"log"
	"net"
	"net/http"
	"nodemessage.com/x-simweb/logic/nat"
	"nodemessage.com/x-simweb/utils"
	"os"
	"path/filepath"
	"strings"
	"sync"
	"sync/atomic"
	"time"
)

type Domain string

type ClientNetKey struct {
	ClientType nat.ClientType
	Uuid       string
	Val        any
}

func NewClientNetKey(clientType nat.ClientType, uuid string, val any) ClientNetKey {
	return ClientNetKey{ClientType: clientType, Uuid: uuid, Val: val}
}

// NatServer nat服务
// virtualPort 虚拟端口,用于域名类型的穿透 默认:8800
// connectPort 服务端连接端口 默认:8801
// maxConnect 最大注册的代理客户端数 默认:20
// curConnect 当前注册的代理客户端数
// rsaKeyPath 密钥路径
// offLineWatchInterval 扫描闲置客户端下线间隔 默认:5s
// offLineLimit 客户端下线超时 默认:10
type NatServer struct {
	virtualPort          int
	connectPort          int
	maxConnect           int32
	curConnect           int32
	rsaKeyPath           string
	offLineWatchInterval time.Duration
	offLineLimit         int32
	logger               *log.Logger

	clientNet         map[ClientNetKey]*nat.Connect
	heartbeatPortChan chan int
	closeLock         sync.Mutex
}

func NewNatServer(logs *log.Logger) *NatServer {
	server := NatServer{
		virtualPort:          8800,
		connectPort:          8801,
		maxConnect:           20,
		logger:               logs,
		clientNet:            make(map[ClientNetKey]*nat.Connect, 5),
		rsaKeyPath:           utils.BinAbsPath(),
		offLineWatchInterval: time.Second * 5,
		offLineLimit:         10,
		heartbeatPortChan:    make(chan int),
	}
	return &server
}

func (n *NatServer) SetOffLineWatchInterval(offLineWatchInterval time.Duration) *NatServer {
	n.offLineWatchInterval = offLineWatchInterval
	return n
}

func (n *NatServer) SetOffLineLimit(offLineLimit int32) *NatServer {
	n.offLineLimit = offLineLimit
	return n
}

func (n *NatServer) SetRsaKeyPath(path string) *NatServer {
	n.rsaKeyPath = path
	return n
}

func (n *NatServer) SetVirtualPort(port int) *NatServer {
	n.virtualPort = port
	return n
}
func (n *NatServer) SetConnectPort(port int) *NatServer {
	n.connectPort = port
	return n
}

func (n *NatServer) SetMaxConnect(max int32) *NatServer {
	n.maxConnect = max
	return n
}

func (n *NatServer) GenRsaKeys(overwrite bool) error {
	keysDir := filepath.Join(n.rsaKeyPath, nat.KeysRelativeDir)
	if !utils.FileIsExist(keysDir) {
		err := os.MkdirAll(keysDir, 0o666)
		if err != nil {
			n.logger.Println("rsa目录生成异常:", keysDir, err)
			return err
		}
	}
	privateKeyPath := filepath.Join(keysDir, "privateKey")
	publicKeyPath := filepath.Join(keysDir, "publicKey")
	if (utils.FileIsExist(privateKeyPath) || utils.FileIsExist(publicKeyPath)) && !overwrite {
		n.logger.Println("rsa公司钥已存在:", keysDir)
		return nat.ErrorRsaKeysExits()
	}
	privateKey, publicKey, err := utils.GenerateRSAKeys(2048)
	if err != nil {
		return err
	}
	err = utils.SavePEMKey(privateKeyPath, privateKey)
	if err != nil {
		n.logger.Println("rsa私钥转存文件异常:", privateKeyPath, err)
		return err
	}
	err = utils.SavePublicPEMKey(publicKeyPath, publicKey)
	if err != nil {
		n.logger.Println("rsa公钥转存文件异常:", publicKeyPath, err)
		return err
	}
	n.logger.Println("rsa公钥转存文件成功:", keysDir)
	return nil
}

func (n *NatServer) clientConnect(connect net.Conn, heartbeatPort int) {
	uuid, err := utils.NewUUID()
	if err != nil {
		n.logger.Println("uuid生成失败:", uuid)
		return
	}
	reader := bufio.NewReader(connect)
	response, err := reader.ReadBytes('\n')
	if err != nil {
		n.logger.Printf("Failed to read response: %v\n", err)
		return
	}
	data := nat.NewConnect()
	err = nat.DecodeDataJson(n.logger, []byte(strings.TrimRight(string(response), "\n")), &data, n.rsaKeyPath)
	if err != nil {
		n.logger.Printf("解密数据异常: %v\n", err)
		return
	}
	data.Uuid = uuid
	key := NewClientNetKey(data.ClientType, uuid, data.Val)
	if _, exits := n.clientNet[key]; exits {
		n.logger.Println("连接已存在:", uuid)
		return
	}
	data.Ins = connect
	n.clientNet[key] = data
	/* 增加连接的客户端 */
	keyJson, err := utils.JsonToStr(&key)
	if err != nil {
		n.logger.Println("认证Key json序列化失败:", *data, err)
		return
	}
	atomic.AddInt32(&n.curConnect, 1)
	result, err := utils.JsonToStr(&nat.AuthPack{
		Uuid:              data.Uuid,
		HeartbeatPort:     heartbeatPort,
		HeartbeatInterval: n.offLineWatchInterval,
		Key:               keyJson,
	})
	if err != nil {
		n.logger.Println("认证Result json序列化失败:", *data, err)
		return
	}
	_, err = data.Ins.Write([]byte(result + "\n"))
	if err != nil {
		n.logger.Println("客户端认证返回消息通知失败:", *data, err)
		return
	}
	n.logger.Printf("客户端连接成功:uuid:%s,ip:%s\n", data.Uuid, data.Ins.RemoteAddr().String())
	_, err = reader.ReadByte()
	if err != nil {
		n.closeClient(key, false)
	}
}

// 启动虚拟端口监听
func (n *NatServer) listenVirtualPort(cancel context.CancelFunc) {
	defer cancel()
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", n.virtualPort))
	if err != nil {
		n.logger.Printf("nat http监听异常:%v\n", err)
		return
	}
	defer listener.Close()
	n.logger.Printf("nat listenVirtual启动成功:%d\n", n.virtualPort)
	for {
		accept, err := listener.Accept()
		if err != nil {
			n.logger.Println("nat listenVirtual,监听到客户端,处理异常,%v", err)
			continue
		}
		go n.handlerVirtualPort(accept)
	}
}
func (n *NatServer) handlerVirtualPort(conn net.Conn) {
	defer conn.Close()
	reader := bufio.NewReader(conn)
	request, err := http.ReadRequest(reader)
	if err != nil {
		if err == io.EOF {
			return
		}
		n.logger.Println("virtualPort读取失败:", err)
		return
	}
	var forwardHost *nat.Connect
	for index, val := range n.clientNet {
		if index.ClientType == nat.TypeDomain && index.Val == request.Host {
			forwardHost = val
			break
		}
	}
	if forwardHost == nil {
		n.logger.Printf("未找到可转发地址,来自地址:%s\n", request.RemoteAddr)
		return
	}
	clientProxyCon, err := net.Dial("tcp", fmt.Sprintf("%s:%d", forwardHost.Ins.RemoteAddr().(*net.TCPAddr).IP, forwardHost.ListenerPort))
	if err != nil {
		n.logger.Printf("nat客户端注册监听异常:%v\n", err)
		return
	}
	defer clientProxyCon.Close()
	err = request.Write(clientProxyCon)
	if err != nil {
		n.logger.Printf("转发http消息失败地址:%s,%v\n", clientProxyCon.RemoteAddr(), err)
		return
	}
	clientReader := bufio.NewReader(clientProxyCon)
	response, err := http.ReadResponse(clientReader, request)
	if err != nil {
		n.logger.Printf("接受转发http消息失败,来自地址:%s,%v\n", forwardHost.Ins.RemoteAddr(), err)
		return
	}
	err = response.Write(conn)
	if err != nil {
		return
	}

}

// 启动客户端注册端口监听
func (n *NatServer) listenConnectPort(cancel context.CancelFunc) {
	defer cancel()
	heartbeatPort := <-n.heartbeatPortChan
	n.logger.Printf("nat heartbeatServer启动成功:%d\n", heartbeatPort)
	listener, err := net.Listen("tcp", fmt.Sprintf(":%d", n.connectPort))
	if err != nil {
		n.logger.Printf("nat客户端注册监听异常:%v\n", err)
		return
	}
	defer listener.Close()
	n.logger.Printf("nat connectServer启动成功:%d\n", n.connectPort)
	for {
		clientConn, err := listener.Accept()
		if err != nil {
			n.logger.Println("nat connectServer,监听到客户端,处理异常,%v", err)
			continue
		}
		if n.curConnect > n.maxConnect {
			n.logger.Println("nat服务端可注册客户端已满,%s", clientConn.RemoteAddr().String())
			clientConn.Close()
			continue
		}
		go n.clientConnect(clientConn, heartbeatPort)
	}
}

func (n *NatServer) offLineWatch() {
	time.AfterFunc(n.offLineWatchInterval, func() {
		n.closeLock.Lock()
		defer n.closeLock.Unlock()
		var discard []ClientNetKey
		for key, connect := range n.clientNet {
			if !connect.HeartbeatUnProcess(n.offLineLimit) {
				discard = append(discard, key)
			}
		}
		for _, key := range discard {
			n.closeClient(key, true)
		}
		go n.offLineWatch()
	})
}

func (n *NatServer) closeClient(key ClientNetKey, force bool) {
	if !force {
		n.closeLock.Lock()
		defer n.closeLock.Unlock()
	}
	connect := n.clientNet[key]
	if connect != nil {
		err := connect.Ins.Close()
		if err != nil {
			n.logger.Printf("客户端闲置地址%s[%s] close异常.%v\n", connect.Ins.RemoteAddr().String(), key.Uuid, err)
		} else {
			delete(n.clientNet, key)
			n.logger.Printf("客户端闲置地址%s[%s] 下线.\n", connect.Ins.RemoteAddr().String(), key.Uuid)
		}
	}
}

func (n *NatServer) listenerHeartbeat(cancel context.CancelFunc) {
	heartbeat := nat.NewServiceHeartbeat(n.logger)
	err := heartbeat.Listener(func(data string) {
		key := ClientNetKey{}
		err := nat.DecodeDataJson(n.logger, []byte(data), &key, n.rsaKeyPath)
		if err != nil {
			n.logger.Printf("心跳数据解析错误:%v\n", err)
			return
		}
		connect := n.clientNet[key]
		if connect != nil {
			connect.HeartbeatProcess()
		}
	}, n.heartbeatPortChan)
	if err != nil {
		cancel()
	}
}

func (n *NatServer) Run() {
	ctx, cancelFunc := context.WithCancel(context.Background())
	go n.listenerHeartbeat(cancelFunc)
	go n.listenVirtualPort(cancelFunc)
	go n.listenConnectPort(cancelFunc)
	go n.offLineWatch()
	<-ctx.Done()
}
