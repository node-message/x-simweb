package nat

import "fmt"

const (
	errorRsaKeysExitsCode = iota + 100
	errorRsaKeysNotfoundCode
)

type ErrorRsaKeys struct {
	code    int
	Message string
}

func (e *ErrorRsaKeys) Error() string {
	return fmt.Sprintf("Code: %d, Message: %s", e.Code(), e.Message)
}

func (e *ErrorRsaKeys) Code() int {
	return e.code
}

func (e *ErrorRsaKeys) IsErrorRsaKeys() bool {
	return e.Code() == errorRsaKeysExitsCode
}

func (e *ErrorRsaKeys) ISErrorRsaKeysNotfound() bool {
	return e.Code() == errorRsaKeysNotfoundCode
}

func ErrorRsaKeysExits() error {
	return &ErrorRsaKeys{
		code:    errorRsaKeysExitsCode,
		Message: "Ras密钥文件已生成",
	}
}

func ErrorRsaKeysNotfound() error {
	return &ErrorRsaKeys{
		code:    errorRsaKeysNotfoundCode,
		Message: "Ras密钥文件不存在",
	}
}
