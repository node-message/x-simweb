package clietn

import (
	"bufio"
	"context"
	"fmt"
	"log"
	"net"
	"net/http"
	"nodemessage.com/x-simweb/logic/nat"
	"nodemessage.com/x-simweb/utils"
	"strings"
)

// NatClient
// host 服务端连接主机
// port 服务端连接端口
// rsaKeyPath 密钥路径
type NatClient struct {
	host       string
	port       int
	rsaKeyPath string

	logger *log.Logger
}

func NewNatClient(logger *log.Logger, connectHost string, connectPort int) *NatClient {
	return &NatClient{host: connectHost, port: connectPort, logger: logger, rsaKeyPath: utils.BinAbsPath()}
}

func (n *NatClient) SetRsaKeyPath(path string) *NatClient {
	n.rsaKeyPath = path
	return n
}

func (n *NatClient) RunWork(clientType nat.ClientType, serverRequestAddr, proxyIpAddr string) {
	ctx, cancelFunc := context.WithCancel(context.Background())
	var portChan = make(chan int, 1)
	go n.listenerProxy(cancelFunc, proxyIpAddr, portChan)
	go n.connectServer(cancelFunc, clientType, serverRequestAddr, portChan)
	<-ctx.Done()
}

func (n *NatClient) listenerProxy(cancelFunc context.CancelFunc, proxyIpAddr string, portChan chan int) {
	listener, err := net.Listen("tcp", ":0")
	if err != nil {
		n.logger.Printf("随机监听失败: %v\n", err)
		cancelFunc()
		return
	}
	defer listener.Close()
	port := listener.Addr().(*net.TCPAddr).Port
	n.logger.Printf("随机监听建立连接%d\n", port)
	portChan <- port
	for {
		accept, err := listener.Accept()
		if err != nil {
			n.logger.Println("listenerProxy,监听到服务端转发消息异常,%v", err)
			continue
		}
		go n.forwardRequest(accept, proxyIpAddr)
	}
}

// 转发到客户端
func (n *NatClient) forwardRequest(conn net.Conn, tcpIp string) {
	// 等待服务端信息
	reader := bufio.NewReader(conn)
	request, err := http.ReadRequest(reader)
	if err != nil {
		n.logger.Printf("%s接受服务端转发消息失败,%v\n", tcpIp, err)
		return
	}
	// 连接proxy
	proxy, err := net.Dial("tcp", tcpIp)
	if err != nil {
		n.logger.Printf("连接proxy:%s失败,%v\n", tcpIp, err)
		return
	}
	defer proxy.Close()
	// 写入proxy
	err = request.Write(proxy)
	if err != nil {
		n.logger.Printf("写入proxy客户端消息%s失败,%v\n", proxy.RemoteAddr().String(), err)
		return
	}

	newReader := bufio.NewReader(proxy)
	response, err := http.ReadResponse(newReader, request)
	if err != nil {
		n.logger.Printf("读取proxy客户端消息%s失败,%v\n", proxy.RemoteAddr().String(), err)
		return
	}
	// 读取
	err = response.Write(conn)
	if err != nil {
		n.logger.Printf("转发到服务端proxy客户端消息%s失败,%v\n", proxy.RemoteAddr().String(), err)
		return
	}
}

func (n *NatClient) connectServer(cancelFunc context.CancelFunc, clientType nat.ClientType, serverRequestAddr string, portChan chan int) {
	tcpIp := fmt.Sprintf("%s:%d", n.host, n.port)
	conn, err := net.Dial("tcp", tcpIp)
	if err != nil {
		n.logger.Printf("服务端连接失败: %s,%v\n", tcpIp, err)
		cancelFunc()
		return
	}
	defer conn.Close()
	n.logger.Printf("服务端建立连接%s\n", tcpIp)
	// Send a message to the server
	uuid, err := utils.NewUUID()
	if err != nil {
		n.logger.Printf("服务连接随机uuid生成失败%s. %v\n", tcpIp, err)
		cancelFunc()
		return
	}
	port := <-portChan
	connect := nat.NewServiceConnect(uuid, clientType, serverRequestAddr, port)
	jsonData, err := nat.EncodeDataJson(n.logger, connect, n.rsaKeyPath)
	if err != nil {
		n.logger.Printf("连接信息加密异常%s. %v\n", tcpIp, err)
		cancelFunc()
		return
	}
	_, err = fmt.Fprintf(conn, string(jsonData)+"\n")
	if err != nil {
		n.logger.Printf("服务端验证信息发送失败%s. %v\n", tcpIp, err)
		cancelFunc()
		return
	}
	reader := bufio.NewReader(conn)
	response, err := reader.ReadString('\n')
	if err != nil {
		n.logger.Printf("服务端验证信息读取失败%s. %v\n", tcpIp, err)
		cancelFunc()
		return
	}
	result := strings.Trim(response, "\n")
	pack := nat.AuthPack{}
	err = utils.JsonParseStr(result, &pack)
	if err != nil {
		n.logger.Printf("服务端验证信息json解析失败%s. %v\n", tcpIp, err)
		cancelFunc()
		return
	}
	connect.Uuid = pack.Uuid
	n.logger.Printf("服务端连接成功%s[%s].\n", tcpIp, connect.Uuid)
	heartbeat := nat.NewClientHeartbeat(n.host, pack.HeartbeatPort, n.logger)
	n.logger.Printf("服务端心跳端口%d,心跳间隔%s\n", pack.HeartbeatPort, pack.HeartbeatInterval.String())
	keyEncode, err := nat.EncodeDataJson(n.logger, pack.Key, n.rsaKeyPath)
	if err != nil {
		n.logger.Printf("心跳启动失败%v\n", err)
		cancelFunc()
		return
	}
	err = heartbeat.StartJump(pack.HeartbeatInterval, keyEncode)
	if err != nil {
		n.logger.Printf("心跳启动失败%v\n", err)
		cancelFunc()
		return
	}
	_, err = reader.ReadByte()
	if err != nil {
		n.logger.Printf("服务端消息丢失%v\n", err)
		cancelFunc()
	}
}
