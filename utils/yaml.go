package utils

import (
	"gopkg.in/yaml.v2"
	"os"
)

func ReadYml(path string, result interface{}) error {
	data, err := os.ReadFile(path)
	if err != nil {
		return err
	}
	if err := yaml.Unmarshal(data, result); err != nil {
		return err
	}
	return nil
}
