package utils

import "encoding/json"

func JsonToStr(obj interface{}) (string, error) {
	marshal, err := json.Marshal(obj)
	if err != nil {
		return "", err
	}
	return string(marshal), nil
}

func JsonParseStr(jsonData string, obj interface{}) error {
	return json.Unmarshal([]byte(jsonData), obj)
}
