### 配置文件详解

1. 代码示例

```go
type Config struct {
/* 端口 */
Port int `yaml:"port"`
/* 域名 */
Domain string `yaml:"domain"`
/* proxy服务 */
Servers []Server `yaml:"servers"`
/* 静态文件路径 */
Files []File `yaml:"files"`
}

type Server struct {
/* 服务地址 */
Target []string `yaml:"target"`
/* 服务前缀 */
ContextPath string `yaml:"contextPath"`
}

type File struct {
/* 目录地址 */
Target string `yaml:"target"`
/* 服务前缀 */
ContextPath string `yaml:"contextPath"`
}
```

2. 配置文件详解
```yaml
port: 1902
files:
  - target: D:\abc #目录地址
    contextPath: /test/ #服务前缀
  - target: E:\abc
    contextPath: /test1/
servers:
  - target: https://www.baidu.com/ #服务地址
    contextPath: /baidu/ #服务前缀
```