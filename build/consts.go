package build

import "log"

const BannerPattern = `  ____ ___ __  ____        _______ ____  
 / ___|_ _|  \/  \ \      / / ____| __ ) 
 \___ \| || |\/| |\ \ /\ / /|  _| |  _ \ 
  ___) | || |  | | \ V  V / | |___| |_) |
 |____/___|_|  |_|  \_/\_/  |_____|____/ %s 
                                         `

var VERSION = "v1.0.0"

var Log *log.Logger
