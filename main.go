package main

import (
	"fmt"
	"github.com/jessevdk/go-flags"
	"nodemessage.com/x-simweb/build"
	"nodemessage.com/x-simweb/config"
	"nodemessage.com/x-simweb/logic/web"
	"nodemessage.com/x-simweb/logs"
	"nodemessage.com/x-simweb/utils"
	"os"
	"time"
)

func main() {
	defer func() {
		if r := recover(); r != nil {
			fmt.Println("运行异常:", r)
		}
	}()
	fmt.Println(fmt.Sprintf(build.BannerPattern, build.VERSION))
	build.Log = logs.Logs(time.Now().Format(time.DateOnly))
	opts := config.Opts{
		Web: web.Opt{
			Port:        80,
			RequestPath: "/",
		},
		NatServer: config.NatServer{
			VirtualPort:          8800,
			ConnectPort:          8801,
			RsaKeyPath:           utils.BinAbsPath(),
			OffLineWatchInterval: time.Second * 5,
			OffLineLimit:         10,
		},
		NatClient: config.NatClient{
			ConnectPort: 8801,
			RsaKeyPath:  utils.BinAbsPath(),
		},
	}
	dir, err := os.Getwd()
	if err != nil {
		build.Log.Println("获取目录异常 ", err)
		return
	}
	build.Log.Println("日志文件路径:", logs.Path)
	opts.Web.Home = dir
	_, err = flags.Parse(&opts)
	if e, ok := err.(config.Ignore); ok {
		build.Log.Println(e.Error())
	}
}
