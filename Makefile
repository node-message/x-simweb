BINARY_NAME=simweb
BINARY_VERSION=v1.0.0.alpha
RACE=-ldflags="-s -w -X "nodemessage.com/x-simweb/build.VERSION=${BINARY_VERSION}""

.PHONY: all
all: win linux

.PHONY: linux
linux:
	GOOS=linux GOARCH=amd64 CGO_ENABLED=0 go build $(RACE) -o ./bin/$(BINARY_NAME)-linux/$(BINARY_NAME) ./main.go

.PHONY: win
win:
	GOOS=windows GOARCH=amd64 go build $(RACE) -o ./bin/$(BINARY_NAME)-win/$(BINARY_NAME).exe ./main.go

install:
	go mod tidy

build: win linux