package config

import (
	"nodemessage.com/x-simweb/build"
	"nodemessage.com/x-simweb/logic/nat"
	"nodemessage.com/x-simweb/logic/nat/clietn"
	"nodemessage.com/x-simweb/logic/nat/server"
	"nodemessage.com/x-simweb/logic/web"
	"time"
)

type Ignore struct {
	msg string
}

func IgnoreError(msg string) Ignore {
	return Ignore{msg: msg}
}

func (i Ignore) Error() string {
	return i.msg
}

type Opts struct {
	Web       web.Opt   `command:"web" description:"web服务器"`
	NatServer NatServer `command:"nats" description:"内网穿透服务端"`
	NatClient NatClient `command:"natc" description:"内网穿透客户端"`
}

type NatServer struct {
	GenerateKey          bool          `short:"g" long:"gen" description:"生成公私钥"`
	RsaKeyPath           string        `short:"k" long:"key" description:"公私钥目录"`
	VirtualPort          int           `short:"v" long:"vport" description:"虚拟监听端口"`
	ConnectPort          int           `short:"c" long:"cport" description:"客户端连接端口"`
	OffLineWatchInterval time.Duration `long:"oinv" description:"客户端下线扫描间隔"`
	OffLineLimit         int32         `long:"olimit" description:"客户端下线超时次数"`
}

func (receiver NatServer) Execute(arg []string) error {
	natServer := server.NewNatServer(build.Log)
	if receiver.GenerateKey {
		natServer.SetRsaKeyPath(receiver.RsaKeyPath)
		err := natServer.GenRsaKeys(true)
		if err != nil {
			return err
		}
	}
	natServer.SetVirtualPort(receiver.VirtualPort)
	natServer.SetConnectPort(receiver.ConnectPort)
	natServer.SetOffLineWatchInterval(receiver.OffLineWatchInterval)
	natServer.SetOffLineLimit(receiver.OffLineLimit)
	natServer.Run()
	return IgnoreError("内网穿透服务端结束")
}

type NatClient struct {
	ConnectHost       string `short:"l" long:"local" description:"服务端连接主机" required:"true"`
	ConnectPort       int    `short:"p" long:"port" description:"服务端连接端口"`
	RsaKeyPath        string `short:"k" long:"key" description:"公钥目录"`
	ServerRequestAddr string `long:"request-addr" description:"公钥目录"  required:"true"`
	ProxyIpAddr       string `long:"proxy-addr" description:"公钥目录"  required:"true"`
}

func (receiver NatClient) Execute(arg []string) error {
	natClient := clietn.NewNatClient(build.Log, receiver.ConnectHost, receiver.ConnectPort)
	natClient.SetRsaKeyPath(receiver.RsaKeyPath)
	natClient.RunWork(nat.TypeDomain, receiver.ServerRequestAddr, receiver.ProxyIpAddr)
	return IgnoreError("内网穿透客户端结束")
}
